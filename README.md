# pineScripts

A collection of bash scripts for the Pinephone. Designed to either make tasks simpler or add functionality.
This is to make it easy on the user to get additional commands/more functionality with the current commands.
If you have any ideas for scripts or would like to contribute, please make an issue and I will see what can be done.

Another goal for this project is to make certain tasks easier by allowing other programs to call upon these scripts.

# Future Goals

- Create basic scripts postmarketOS
- Create basic scripts for PureOS/Librem 5
- man pages for each script

As a whole these should be fairly easy, but may need individual tweaks for each individual OS. 
For example, Mobian allows apt update/upgrade where as UBTouch has var lock which must be disabled before the commands can be run.

# Script Set Up

For mobian and other OSes that are quite similar to desktop Linux, please utilize the master branch. For UBTouch, utilize its specific branch, because it chooses to lock down the file system, so the scripts need to be adjusted to work. 

Download set up file and in terminal cd to the directory the download is in and run:
```
sudo apt install git
bash ./setUp
```

# Use
After initial set up, use is very simple. Input the script name into the terminal from anywhere to call that script.
For example, input update into the terminal and run it. This will run sudo apt update && sudo apt upgrade for the user.

